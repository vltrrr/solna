# Solna #

Solna is a set of python tools to process and plot data acquired from microscopy image sequences of cellulose fibre dissolution.

![fiber](img/fiber.png)


## Installation ##

Requirements:

* Python3
* NumPy
* SciPy
* Matplotlib
* Pandas
* Joblib (video processing)
* skikit-image (video processing)
* imageio (video processing)

In Debian based distros, you can start with:

```
#!bash

apt-get install python3 python-virtualenv
```

And let pip take care of the rest in a dedicated virtualenv. To create a new virtualenv and activate it (you probably want to install to virtualenv):

```
#!bash
virtualenv --python=python3 solna
source solna/bin/activate

```

Finally dowload the wheel package and use pip to install it:

```
#!bash

pip install solna-x.x.x-py3-none-any.whl

```

Or alternatively clone the repo and install the package in editable mode

```
#!bash

git clone https://vltrrr@bitbucket.org/vltrrr/solna.git solna
pip install -e solna/src/

```

## Examples ##

```
#!bash

# extract ImageJ produced csv:s into numpy NPZ, and set correct framerate (frame interval 1.5 sec)
solna_extr data/csvs_in_here/ -framei 1.5
```

```
#!bash

# process the dataset (csvs_in_here.npz) with some sample options:
# use smoothing
# time/x-axis maximum 700sec
# cluster with a threshold of 175 sec
# legend names for clusters
# produce png plots
solna_proc -smooth -maxtime 700 -cluster_time 175 -cluster_names Fast:Slow -png csvs_in_here.npz
```


## Options ##

### solna_extr ###

Extract fiber data imageJ CSV files

```
#!bash

$ solna_extr -h
solna_extract (Solna 0.6.3 2018_02_26 python 3.5.2 Linux)

usage: solna_extr [-h] [-dlm DELIM] [-mat] [-framei FRAMEI] [-framer FRAMER]
                  csv_dir

positional arguments:
  csv_dir         csv

optional arguments:
  -h, --help      show this help message and exit
  -dlm DELIM      delimiter
  -mat            output data in .mat files
  -framei FRAMEI  original/force frame interval
  -framer FRAMER  original/force frame rate
```


### solna_vview (experimental) ###

View video files (with post processing):

```
#!bash

$ solna_vview -h
solna_video_view (Solna 0.6.4 2018_02_26 python 3.5.2 Linux)

usage: solna_vview [-h] [-cmap {gray,magma,inferno}] [-framei FRAMEI]
                   [-fnum FNUM] [-tresh_sauvola] [-eqhist {adaptive,global}]
                   in_file

positional arguments:
  in_file               input video file

optional arguments:
  -h, --help            show this help message and exit
  -cmap {gray,magma,inferno}
                        colormap
  -framei FRAMEI        animation frame interval in ms
  -fnum FNUM            show only frame n
  -tresh_sauvola        use sauvola theshold
  -eqhist {adaptive,global}
                        apply histogram equalization
```


### solna_vextr (experimental) ###

Extract fiber data directly from video files

```
#!bash

$ solna_extr -h
solna_video_extract (Solna 0.6.3 2018_02_26 python 3.5.2 Linux)

usage: solna_vextr [-h] [-framei FRAMEI] [-framer FRAMER] input

positional arguments:
  input           input file or directory

optional arguments:
  -h, --help      show this help message and exit
  -framei FRAMEI  original/force frame interval
  -framer FRAMER  original/force frame rate
```


### solna_proc ###

```
#!bash

$ solna_proc -h
solna_proc (Solna 0.6.3 2018_02_26 python 3.5.2 Linux)

usage: solna_proc [-h] [-maxtime MAXTIME] [-smooth] [-smooth_size MAVG_SIZE]
                  [-cluster N_CLUSTERS] [-cluster_hard CLUSTER_HARD]
                  [-cluster_gradmax CLUSTER_GRADMAX]
                  [-cluster_time CLUSTER_TIME] [-cluster_file CLUSTER_FILE]
                  [-cluster_names CLUSTER_NAMES]
                  [-cluster_colors CLUSTER_COLORS] [-out_cnames] [-plt]
                  [-plt_legend] [-plt_cumsum] [-plt_gradients] [-plt_ci]
                  [-plt_initspeed] [-plt_expfit] [-plt_logy] [-plt_x PLT_X]
                  [-plt_y PLT_Y] [-png] [-pdf] [-mat] [-csv_avg] [-csv_lowres]
                  npz_file

positional arguments:
  npz_file              .npz dataset file

optional arguments:
  -h, --help            show this help message and exit
  -maxtime MAXTIME      interpolate time to x s (default maximum encountered)
  -smooth               mavg/boxcar smooth
  -smooth_size MAVG_SIZE
                        mavg/boxcar smooth size (in 10ms resolution
                        datapoints)
  -cluster N_CLUSTERS   k mean clustering to given nuber of clusters
  -cluster_hard CLUSTER_HARD
                        cumsum clustering with hard limit
  -cluster_gradmax CLUSTER_GRADMAX
                        clustering with absolute dissolution speed (px/sec)
  -cluster_time CLUSTER_TIME
                        clustering cutoff time (where max 0.1 of relative area
                        left)
  -cluster_file CLUSTER_FILE
                        manual clustering read from file
  -cluster_names CLUSTER_NAMES
                        cluster names for legend, separated with :
  -cluster_colors CLUSTER_COLORS
                        cluster colors, separated with :
  -out_cnames           cluster names to csv output
  -plt                  view plot
  -plt_legend           show legend
  -plt_cumsum           plot cumulative sum
  -plt_gradients        plot gradients
  -plt_ci               plot confidence interval
  -plt_initspeed        plot initial speed
  -plt_expfit           plot exponential fit
  -plt_logy             logarithmic y axis
  -plt_x PLT_X          plot x dimension in mm
  -plt_y PLT_Y          plot y dimension in mm
  -png                  write png figures
  -pdf                  write pdf figures
  -mat                  output data in .mat files
  -csv_avg              output low res trace averages in csv
  -csv_lowres           output low res traces in csv
```


# Building #

Create a new virtualenv if needed & activate it. Go to the dir, build & install the wheel package

```
#!bash
cd solna/code/

# build
python3 setup.py bdist_wheel


```

# License #

BSD 3-Clause. See:
https://opensource.org/licenses/BSD-3-Clause


Copyright 2017 Valtteri M�kel�

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
