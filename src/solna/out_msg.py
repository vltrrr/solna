import os
import collections

import numpy as np
import scipy.io

from solna.common import *
from solna import out_csv


# ____________________________________________________________________________
# main printouts

def print_fits(dset):
    msg('\t# inititial speed y = k*t + c')
    msg('\t# relative k (%/sec)  c (sec)')
    msg('\t{:1.2f}    {:1.2f}'.format(dset.fit_initspeed, dset.fit_intercept))
    msg('\t# absolute k (px/sec) c (sec)')
    msg('\t{:1.2f}    {:1.2f}'.format(dset.fit_initspeed_abs, dset.fit_intercept_abs))
    msg('\t# expfit y = a * exp(-t/t_0)')
    msg('\t# relative a (%)    t0 (sec)    S (stderr of regression)')
    msg('\t{:1.2f}    {:1.2f}    {:1.4f}'.format(
        dset.fit_exp_coefs[0],
        dset.fit_exp_coefs[1],
        dset.fit_exp_stderr)
        )
    msg('\t# absolute a (pix)  t0 (sec)    S (stderr of regression)')
    msg('\t{:1.2f}    {:1.2f}    {:1.4f}'.format(
        dset.fit_exp_coefs_abs[0],
        dset.fit_exp_coefs_abs[1],
        dset.fit_exp_stderr_abs)
        )
    msg('\t# fiber avg/median size (px)')
    msg('\t{:1.2f}    {:1.2f}'.format(dset.area_size_avg, dset.area_size_median))


def print_avg_diss_stats(dset):
    msg('\t# mean   median  stderr  (sec)')
    msg('\t{:1.2f}  {:1.2f}  {:1.2f}'.format(*dset.diss_times_stats))
    msg('\t# confidence interval (95%)')
    msg('\t{:1.2f} - {:1.2f} sec'.format(*dset.diss_times_ci95))


# ____________________________________________________________________________
# cluster printouts

def print_cluster_diss_limits(diss_limits):
    msg('\t# 25%   50%   75%   95%   99.5%')
    for i, dlimit in enumerate(diss_limits):
        msg('\t{:2d}:  {:5.2f}  {:5.2f}  {:5.2f}  {:5.2f}  {:5.2f}'.format(
                    i+1,
                    *dlimit
                )
            )


def print_cluster_fits(dset):
    msg('\t# expfit')
    msg('\t# a    t0    S (stderr of regression)')
    for i, coefs in enumerate(dset.cluster_fit_exp_coefs):
        msg('\t {:d}: {:1.2f}  {:1.2f}  {:1.4f}'.format(i+1, *coefs, dset.cluster_fit_exp_stderrs[i]))


def print_cluster_avg_diss_stats(dset):
    msg('\t# avg  median  stderr (sec)')
    for i, stats in enumerate(dset.cluster_diss_times_stats):
        msg('\t {:d}: {:1.2f}  {:1.2f}  {:1.2f}'.format(i+1, *stats))

    msg('\t# confidence interval (95%)')
    for i, ci95 in enumerate(dset.cluster_diss_times_ci95):
        msg('\t {:d}: {:1.2f} - {:1.2f} sec'.format(i+1, *ci95))


def print_cluster_sizes(codes):
    n_of_clusters, counts = out_csv.calc_cluster_sizes(codes)

    for c in range(n_of_clusters):
        msg('\t{:2d}:  {:d}'.format(c+1, counts[c]))


def print_cluster_sample_codes(names, codes):
    n_of_clusters = np.amax(codes) + 1

    for c in range(n_of_clusters):
        for name, code in zip(names, codes):
            if code == c:
                msg('\t{:2d}  {:s} '.format(code+1, name))
