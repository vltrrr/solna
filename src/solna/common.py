import sys

# ____________________________________________________________________________
# common functions msg/error handling

SOLNA_VERSION = 'Solna 0.6.4 2018_02_26'


def msg(message):
    print(message)


def solna_error(message):
    msg('\nError: {:s}'.format(message))
    sys.exit(1)


def solna_execute(main_func):
    def wrapper(*args, **kwargs):
        try:
            main_func(*args, **kwargs)
        except KeyboardInterrupt:
            print('\nInterupted, aborting..\n')
        except SystemExit:
            print('')
        except BaseException as ex:
            print(ex)
            print('')
            import traceback
            traceback.print_exc()
            print('\nExiting..\n')

    return wrapper
