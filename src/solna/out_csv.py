import os
import collections

import numpy as np
import scipy.io

from solna.common import *


# ____________________________________________________________________________
# helpers

def write_init(out_file, header_str):
    write_header = False
    if not os.path.exists(out_file):
        write_header = True

    handle = open(out_file, 'a')
    if write_header:
        handle.write(header_str)

    return handle


def calc_cluster_sizes(codes):
    n_of_clusters = np.amax(codes) + 1
    counts = np.bincount(codes)

    return (n_of_clusters, counts)


# ____________________________________________________________________________
# fits

def write_fits(dset, out_file):
    handle = write_init(
        out_file,
        '"fit_results";'
        '"init_speed";"intercept";'
        '"init_speed_abs";"intercept_abs";'
        '"expfit_a";"expfit_t0";"expfit_stderr";'
        '"expfit_a_abs";"expfit_t0_abs";"expfit_abs_stderr";'
        '"avg_size_px";"median_size_px";'
        '\n'
    )

    handle.write('{:s};'.format(dset.name))
    handle.write('{:f};'.format(dset.fit_initspeed))
    handle.write('{:f};'.format(dset.fit_intercept))
    handle.write('{:f};'.format(dset.fit_initspeed_abs))
    handle.write('{:f};'.format(dset.fit_intercept_abs))
    handle.write('{:f};{:f};'.format(*dset.fit_exp_coefs))
    handle.write('{:f};'.format(dset.fit_exp_stderr))
    handle.write('{:f};{:f};'.format(*dset.fit_exp_coefs_abs))
    handle.write('{:f};'.format(dset.fit_exp_stderr_abs))
    handle.write('{:f};'.format(dset.area_size_avg))
    handle.write('{:f};'.format(dset.area_size_median))
    handle.write('\n')
    handle.close()


def _generate_cluster_fits_heading(n_of_clusters):
    init_str_list = ['"cluster_fits";']

    for i in range(n_of_clusters):
        init_str_list.append('"expfit_a";"expfit_t0";"expfit_stderr";')
    init_str_list.append('\n')

    return(''.join(init_str_list))


def write_cluster_fits(cluster_fits, cluster_stderrs, dset_name, out_file):
    n_of_clusters = len(cluster_fits)

    header = _generate_cluster_fits_heading(n_of_clusters)
    handle = write_init(out_file, header)

    handle.write('"{:s}";'.format(dset_name))
    for exp_coefs, stderr in zip(cluster_fits, cluster_stderrs):
        handle.write('{:f};{:f};{:f};'.format(*exp_coefs, stderr))

    handle.write('\n')
    handle.close()


# ____________________________________________________________________________
# dissolution limits

def write_cluster_diss_limits(diss_limits, dset_name, out_file, cluster_names=None):
    if cluster_names:
        n_of_clusters = len(cluster_names)
    else:
        n_of_clusters = diss_limits.shape[0]

    header = _generate_diss_limits_heading(n_of_clusters)

    if n_of_clusters > 1:
        cluster_header = _generate_cluster_heading(n_of_clusters, cluster_names)
        header = cluster_header + header

    handle = write_init(out_file, header)

    handle.write('{:s};'.format(dset_name))
    for dslimit in diss_limits:
        handle.write('{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};'.format(*dslimit))

    handle.write('\n')
    handle.close()


def _generate_diss_limits_heading(n_of_clusters):
    init_str_list = []
    init_str_list.append('"dissolution_times";')

    for i in range(n_of_clusters):
        init_str_list.append('"sol_25";"sol_50";"sol_75";"sol_95";"sol_995";')
    init_str_list.append('\n')

    return(''.join(init_str_list))


def _generate_cluster_heading(n_of_clusters, cluster_names):
    header_list = []
    header_list.append('"clusters";')

    if cluster_names:
        for cname in cluster_names:
            header_list.append('"{:s}";"";"";"";"";'.format(cname))
    else:
        for i in range(n_of_clusters):
            header_list.append('"cluster_{:d}";"";"";"";"";'.format(i+1))
    header_list.append('\n')

    return(''.join(header_list))


# ____________________________________________________________________________
# avg stats

def _generate_avg_diss_stats_heading(n_of_clusters):
    init_str_list = []
    init_str_list.append('"dissolution_stats";')

    for i in range(n_of_clusters):
        init_str_list.append('"average_time";"median_time";"std_err";"ci_start";"ci_end";')
    init_str_list.append('\n')

    return(''.join(init_str_list))


def write_avg_diss_stats(diss_stats, diss_ci, dset_name, out_file, cluster_names=None):
    if cluster_names:
        n_of_clusters = len(cluster_names)
    else:
        n_of_clusters = len(diss_stats)

    header = _generate_avg_diss_stats_heading(n_of_clusters)

    if n_of_clusters > 1:
        cluster_header = _generate_cluster_heading(n_of_clusters, cluster_names)
        header = cluster_header + header
    handle = write_init(out_file, header)

    handle.write('"{:s}";'.format(dset_name))
    for stats, ci in zip(diss_stats, diss_ci):
        handle.write('{:.2f};{:.2f};{:.2f};{:.2f};{:.2f};'.format(*stats, *ci))

    handle.write('\n')
    handle.close()


# ____________________________________________________________________________
# cluster sizes

def write_cluster_sizes(codes, dset_name, out_file, cluster_names=None):
    n_of_clusters, counts = calc_cluster_sizes(codes)

    header_build = ['"cluster_sizes";']

    if cluster_names is not None:
        for cname in cluster_names:
            header_build.append('"{:s}";'.format(cname))
    else:
        for i in range(n_of_clusters):
            header_build.append('"cluster_{:d}";'.format(i+1))
    header_build.append('\n')

    handle = write_init(out_file, "".join(header_build))

    handle.write('"{:s}";'.format(dset_name))
    for c in range(n_of_clusters):
        handle.write('{:d};'.format(counts[c]))

    handle.write('\n')
    handle.close()


# ____________________________________________________________________________
# traces

def write_avg_resampled(area_avg, dset_name, out_file):
    handle = write_init(out_file, 'average_traces;\n')

    handle.write('"{:s}";'.format(dset_name))
    for avg in area_avg:
        handle.write("{:f};".format(avg))

    handle.write("\n")
    handle.close()


def write_traces_resampled(trace_areas, trace_names, out_file):
    handle = open(out_file, 'w')
    handle.write('all_traces;\n')

    _write_traces_to_handle(handle, trace_areas, trace_names)
    handle.close()


def write_cluster_avgs_resampled(cluster_avgs, out_file, cluster_names=None):
    n_of_clusters = cluster_avgs.shape[0]
    if cluster_names is None:
        cluster_names = []
        for i in range(n_of_clusters):
            cluster_names.append('cluster_{:d}'.format(i+1))

    handle = open(out_file, 'w')
    handle.write('cluster_avg_traces;\n')
    _write_traces_to_handle(handle, cluster_avgs, cluster_names)
    handle.close()


def _write_traces_to_handle(handle, trace_areas, trace_names):
    for i in range(trace_areas.shape[0]):
        handle.write('"{:s}";'.format(trace_names[i]))
        for val in trace_areas[i, :]:
            handle.write('{:f};'.format(val))
        handle.write('\n')
