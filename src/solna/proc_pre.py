import numpy as np

from solna.common import *


# ____________________________________________________________________________
# pre-processing

def parse_timeaxis_imagej(dframe):
    sec_list = []
    for label in dframe['label']:
        label_split = label.split(":")
        if len(label_split) != 2:
            solna_error('cant parse time: {:s}'.format(label))

        sec_split = label_split[1].split()
        sec = float(sec_split[0])
        sec_list.append(sec)

    sec_arr = np.array(sec_list)
    sec_arr = sec_arr - sec_arr[0]  # start from zero
    dframe['sec'] = sec_arr

    return dframe


def calc_timeaxis(dframe, frame_interval):
    """
    Calculate new time axis from given frame interval
    """

    rows = dframe.shape[0]
    end = (rows * frame_interval) - frame_interval
    dframe['sec'] = np.linspace(0, end, num=rows)

    return dframe


def calc_area_abs_imagej(dframe):
    dframe['area_abs'] = dframe['area_frac'] * 0.01 * dframe['area'][0]
    return dframe


def calc_area_frac(dframe):
    dframe['area_frac'] = dframe['area_abs'] / np.amax(dframe['area_abs'])
    return dframe


def normalize_area_frac(dframe):
    area_frac = dframe['area_frac']
    area_frac = area_frac - np.amin(area_frac)
    dframe['area_frac'] = area_frac / np.amax(area_frac)

    return dframe


def normalize_area_abs(dframe):
    dframe['area_abs'] = dframe['area_abs'] - np.amin(dframe['area_abs'])
    return dframe
