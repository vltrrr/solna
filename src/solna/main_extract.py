import os
import platform
import argparse

import numpy as np

from solna.common import *
from solna import io as sio
from solna import proc, core_extract


# ____________________________________________________________________________
# cmdline / conf

def parse_cmdline():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "in_path",
        help="csv",
        metavar="csv_dir")
    parser.add_argument(
        "-dlm",
        dest="delim",
        help="delimiter",
        default=",")
    parser.add_argument(
        "-mat",
        dest="matlab_enabled",
        help="output data in .mat files",
        default=False,
        action='store_true')

    core_extract.add_default_args(parser)
    args = parser.parse_args()
    core_extract.parse_frame_interval(args)

    return args


# ____________________________________________________________________________
# main

@solna_execute
def main():
    msg('solna_extract ({:s} python {:s} {:s})'.format(SOLNA_VERSION, platform.python_version(), platform.system()))
    msg('')

    conf = parse_cmdline()
    if conf.frame_interval is not None:
        msg('frame interval: {:f}'.format(conf.frame_interval))

    if os.path.isdir(conf.in_path):
        msg('reading dir:')
        file_list, name_list = sio.read_dir_filelist(conf.in_path, suffix_filter='.csv')
        dframe_list = core_extract.read_imagej_csv_list(file_list, conf.delim)
        preproc_dframes = core_extract.preprocess_imagej_dframes(dframe_list, conf.frame_interval)
        name_list = sio.filter_specialchars_list(name_list)

        fname = sio.get_basename(conf.in_path)
        sio.write_npz_dataset(fname, name_list, preproc_dframes, mat_format=conf.matlab_enabled)
    else:
        solna_error('directory not found or supplied path is not a directory.')


# ____________________________________________________________________________
# functions: clustertemplate

def create_cluster_template(in_path):
    filelist = sio.read_dir_filelist(in_path)
    for f in filelist:
        print("{:};0".format(f))


def print_cluster_file(in_path):
    clusters = sio.read_cluster_csv(in_path)
    print(clusters)


@solna_execute
def main_cluster_template():
    msg("solna_cluster_template ({:s} python {:s} {:s})"
        .format(
            SOLNA_VERSION,
            platform.python_version(),
            platform.system())
        )
    msg("")

    parser = argparse.ArgumentParser()
    parser.add_argument("in_path", metavar="input_dir", help="csv directory")
    conf = parser.parse_args()

    if os.path.isfile(conf.in_path):
        print_cluster_file(conf.in_path)
    elif os.path.isdir(conf.in_path):
        create_cluster_template(conf.in_path)
    else:
        solna_error("Supplied path not found.")


if __name__ == "__main__":
    main()
