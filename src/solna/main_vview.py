import os
import platform
import argparse

import numpy as np
import pandas as pd

from solna.common import *
from solna import io as sio
from solna import proc, proc_img, core_extract, plot


# ____________________________________________________________________________
# cmdline / conf

def parse_cmdline():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'in_path',
        help='input video file',
        metavar='in_file')
    parser.add_argument(
        '-cmap',
        help='colormap',
        choices=['gray', 'magma', 'inferno'],
        default='magma')
    parser.add_argument(
        '-framei',
        type=float,
        help='animation frame interval in ms',
        default=50)
    parser.add_argument(
        '-fnum',
        type=int,
        help='show only frame n',
        default=None)
    parser.add_argument(
        '-tresh_sauvola',
        default=False,
        action='store_true',
        help='use sauvola theshold')
    parser.add_argument(
        '-eqhist',
        help='apply histogram equalization',
        default=None,
        choices=['adaptive', 'global'])

    args = parser.parse_args()
    return args


# ____________________________________________________________________________
# main

@solna_execute
def main():
    msg('solna_video_view ({:s} python {:s} {:s})'.format(
            SOLNA_VERSION,
            platform.python_version(),
            platform.system())
        )
    msg('')

    args = parse_cmdline()

    if not os.path.isdir(args.in_path):
        msg('\treading {:s}'.format(args.in_path))
        img_list = core_extract.read_video_file(args.in_path)

        if args.fnum is not None:
            if args.fnum >= len(img_list):
                solna_error('nonexistent frame')
            img_list = [img_list[args.fnum]]

        if args.eqhist:
            img_list = proc_img.apply_equalize_hist(img_list, mode=args.eqhist)

        if args.tresh_sauvola:
            msg('\tapplying sauvola threshold..')
            img_list = proc_img.apply_threshold_sauvola_parallel(img_list)
            msg('\tpixel counts:')
            msg(str(proc_img.count_pixel_sum(img_list)))

        plot.plot_image_sequence(img_list, interval=args.framei, cmap=args.cmap)
    else:
        solna_error('image sequence directories not implemented')

if __name__ == '__main__':
    main()
