import os
import platform
import argparse

import numpy as np

from solna.common import *
from solna import dataset, core, plot
from solna import io as sio


# ____________________________________________________________________________
# cmdline / conf

def parse_cmdline():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "in_path",
        metavar="npz_file",
        help=".npz dataset file")
    parser.add_argument(
        "-maxtime",
        help="interpolate time to x s (default maximum encountered)",
        default=-1,
        type=int)
    parser.add_argument(
        "-smooth",
        dest="mavg_enabled",
        help="mavg/boxcar smooth",
        default=False,
        action='store_true')
    parser.add_argument(
        "-smooth_size",
        dest="mavg_size",
        help="mavg/boxcar smooth size (in 10ms resolution datapoints)",
        default=1201,
        type=int)
    parser.add_argument(
        "-cluster",
        dest="n_clusters",
        help="k mean clustering to given nuber of clusters",
        default=-1,
        type=int)
    parser.add_argument(
        "-cluster_hard",
        help="cumsum clustering with hard limit",
        default=-1.0,
        type=float)
    parser.add_argument(
        "-cluster_gradmax",
        help="clustering with absolute dissolution speed (px/sec)",
        default=-1.0,
        type=float)
    parser.add_argument(
        "-cluster_time",
        help="clustering cutoff time (where max 0.1 of relative area left)",
        default=-1.0,
        type=float)
    parser.add_argument(
        "-cluster_file",
        help="manual clustering read from file",
        default=None)
    parser.add_argument(
        "-cluster_names",
        help="cluster names for legend, separated with :",
        default=None)
    parser.add_argument(
        "-cluster_colors",
        help="cluster colors, separated with :",
        default=None)
    parser.add_argument(
        "-out_cnames",
        dest="cnames_out_enabled",
        help="cluster names to csv output",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt",
        dest="out_plot",
        help="view plot",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_legend",
        dest="plt_legend",
        help="show legend",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_cumsum",
        dest="cumsum_enabled",
        help="plot cumulative sum",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_gradients",
        dest="gradients_enabled",
        help="plot gradients",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_ci",
        dest="plt_ci",
        help="plot confidence interval",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_initspeed",
        dest="plt_initspeed",
        help="plot initial speed",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_expfit",
        dest="plt_expfit",
        help="plot exponential fit",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_logy",
        dest="plt_log_y",
        help="logarithmic y axis",
        default=False,
        action='store_true')
    parser.add_argument(
        "-plt_x",
        help="plot x dimension in mm",
        type=int,
        default=300)
    parser.add_argument(
        "-plt_y",
        help="plot y dimension in mm",
        type=int,
        default=160)
    parser.add_argument(
        "-png",
        dest="out_file_png",
        help="write png figures",
        default=False,
        action='store_true')
    parser.add_argument(
        "-pdf",
        dest="out_file_pdf",
        help="write pdf figures",
        default=False,
        action='store_true')
    parser.add_argument(
        "-mat",
        dest="matlab_enabled",
        help="output data in .mat files",
        default=False,
        action='store_true')
    parser.add_argument(
        "-csv_avg",
        dest="csv_avg_enabled",
        help="output low res trace averages in csv",
        default=False,
        action='store_true')
    parser.add_argument(
        "-csv_lowres",
        dest="csv_lowres_enabled",
        help="output low res traces in csv",
        default=False,
        action='store_true')

    args = parser.parse_args()
    cmdline_check_args(args)

    return args


def cmdline_check_args(args):
    args.plot_enabled = False
    if args.out_plot:
        args.plot_enabled = True
    elif args.out_file_png:
        args.plot_enabled = True
    elif args.out_file_pdf:
        args.plot_enabled = True

    args.cluster_enabled = False
    if args.n_clusters >= 1:
        args.cluster_enabled = True
    elif args.cluster_hard > 0.0:
        args.cluster_enabled = True
    elif args.cluster_gradmax > 0.0:
        args.cluster_enabled = True
    elif args.cluster_time > 0.0:
        args.cluster_enabled = True
    elif args.cluster_file is not None:
        args.cluster_enabled = True

    if args.cluster_names is not None:
        args.cluster_names = args.cluster_names.split(":")

    if args.cluster_colors is not None:
        args.cluster_colors = args.cluster_colors.split(":")


# ____________________________________________________________________________
# main functions

def solna_proc_dir(conf):
    """
    process all npz datasets in a directory
    """
    npz_dataset_combined, auto_cluster_codes = sio.read_npz_datasets_from_dir(conf.in_path)
    solna_proc_main(conf, npz_dataset_combined, auto_cluster_codes)


def solna_proc_single(conf):
    """
    process a single npz dataset
    """
    npz_dataset = sio.read_npz_dataset(conf.in_path)
    solna_proc_main(conf, npz_dataset)


def solna_proc_main(conf, npz_dataset, auto_cluster_codes=None):
    """
    main function: process a raw npz dataset (dictionary of ndarray for each fiber)
    """
    if len(npz_dataset) < 1:
        solna_error('Empty dataset (not enough correct input files found?)')

    msg('prepare data:')
    dset = core.prepare_dataset(npz_dataset, conf)
    core.process_dataset(dset, conf)
    core.write_basic_results(dset, conf)

    # prefill automated cluster codes and enable clustering.
    # will be overwritten with other clustering methods
    if auto_cluster_codes is not None:
        conf.cluster_enabled = True
        dset.cluster_codes = auto_cluster_codes

    if conf.cluster_enabled:
        core.process_clustering(dset, conf)
        core.check_cluster_names_and_colors(dset.cluster_codes, conf.cluster_names, conf.cluster_colors)
        dset.plot_colors = plot.create_colors_codes(dset.cluster_codes, conf.cluster_colors)
        core.write_cluster_results(dset, conf)
    else:
        dset.plot_colors = plot.create_colors_single(dset.areas.shape[0])

    if conf.matlab_enabled:
        sio.write_solna_dataset_matlab("{:s}_proc".format(dset.name), dset, conf.cluster_names)

    if conf.plot_enabled:
        core.plot_basic(dset, conf)
        if conf.cluster_enabled:
            core.plot_cluster(dset, conf)
            core.plot_cluster_gradients(dset, conf)
        if conf.gradients_enabled:
            core.plot_gradients(dset, conf)
        if conf.cumsum_enabled:
            core.plot_cumsum(dset, conf)


# ____________________________________________________________________________
# main

@solna_execute
def main():
    msg("solna_proc ({:s} python {:s} {:s})".format(SOLNA_VERSION, platform.python_version(), platform.system()))
    msg("")

    conf = parse_cmdline()

    if os.path.isfile(conf.in_path):
        solna_proc_single(conf)
    elif os.path.isdir(conf.in_path):
        solna_proc_dir(conf)
    else:
        solna_error("Supplied path not found.")


if __name__ == "__main__":
    main()
