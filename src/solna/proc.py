import numpy as np
from scipy import signal
from scipy.cluster.vq import kmeans, vq
from scipy.stats import t
from scipy.optimize import curve_fit

from solna.common import *


# ____________________________________________________________________________
# process

def resample_timeaxis(max_time, oversamp_rate=100):
    steps = int(max_time) * oversamp_rate + 1
    return np.linspace(0.0, max_time, num=steps)


def resample_area(new_time_axis, time_arr, area_arr):
    return np.interp(new_time_axis, time_arr, area_arr, left=1.0, right=0.0)


def resample_area_lowres(time_arr, area_arr):
    """
    Resample to 1 sample / sec
    """
    maxtime = np.floor(np.amax(time_arr))
    timeaxis_sec = np.linspace(0.0, maxtime, num=int(maxtime+1))
    resampled = resample_area(timeaxis_sec, time_arr, area_arr)
    return (timeaxis_sec, resampled)


def smooth_mavg(areaf, win_len, post_normz=True):
    """
    calc moving average. only 1D arrays as np.convolve is 1D.
    Padding with first value to left and zero to right.
    """

    if win_len % 2 != 1:
        solna_error("use odd number of points in window.")

    win = np.ones(win_len)
    orig_shape = areaf.shape

    pad = int(win_len / 2)
    areaf = np.hstack((np.ones(pad)*areaf[0], areaf))
    areaf = np.hstack((areaf, np.zeros(pad)))

    normz = 1.0 / np.sum(win)
    # use "valid" instead of "same" as padding has been done
    # smoothed = np.convolve(areaf, win, mode="valid") * normz
    smoothed = signal.fftconvolve(areaf, win, mode="valid") * normz

    if post_normz:
        smoothed = smoothed / np.amax(smoothed)

    return smoothed


def gradients(areas):
    gradients = -np.diff(areas, axis=1)
    return np.vstack((gradients.T, np.zeros(gradients.shape[0]))).T


# ____________________________________________________________________________
# fit

def crop_fit_area(y_var, time, limit_hi=1.0, limit_lo=0.0):
    max_value = np.amax(y_var)
    limit_hi = limit_hi * max_value
    limit_lo = limit_lo * max_value

    start_point = np.argmin(np.abs(y_var - limit_hi))
    end_point = np.argmin(np.abs(y_var - limit_lo)) + 1
    time_crop = time[start_point:end_point]
    y_var_crop = y_var[start_point:end_point]

    return y_var_crop, time_crop


def fit_initspeed(area, time, fit_limit):
    """
    fit_limit is the threshold which is used to cut the initial portion of the data.
    Subsequent values after the limit are ignored.
    """
    initial_area, initial_time = crop_fit_area(area, time, limit_lo=fit_limit)
    return np.polyfit(initial_time, initial_area, 1)


def fit_exp(y_var, time, limit_hi=1.0, limit_lo=0.0):
    def exp_fun(x, a, t0):
        return a * np.exp(-x/t0)

    y_var, time = crop_fit_area(y_var, time, limit_hi=limit_hi, limit_lo=limit_lo)
    popt, pcov = curve_fit(exp_fun, time, y_var)
    return popt


def calc_exp_line(a, t0, time):
    return a * np.exp(-time/t0)


def calc_fit_stderr(fit, data):
    """
    fit standared error of regression / estimate
    """
    residuals = np.abs(fit - data)
    return np.std(residuals)


def calc_cluster_exp_fits(cluster_traces, time, limit_hi=1.0, limit_lo=0.0):
    fit_coefs = []
    for i in range(cluster_traces.shape[0]):
        fit_coefs.append(
            fit_exp(cluster_traces[i, :], time, limit_hi=limit_hi, limit_lo=limit_lo)
        )
    return fit_coefs


def calc_cluster_exp_fit_lines(exp_coefs, time):
    fit_lines = []
    for coefs_tuple in exp_coefs:
        fit_lines.append(
            calc_exp_line(*coefs_tuple, time)
        )
    return fit_lines


def calc_cluster_exp_fit_stderrs(exp_lines, cluster_traces):
    std_errs = []
    for i, fit_line in enumerate(exp_lines):
        std_errs.append(
            calc_fit_stderr(fit_line, cluster_traces[i, :])
        )
    return std_errs


# ____________________________________________________________________________
# cluster

def cluster_k_means(numbers, n_of_clusters):
    centoids, distort = kmeans(numbers, n_of_clusters)
    centoids = np.sort(centoids)
    codes, distances = vq(numbers, centoids)

    return codes


# ____________________________________________________________________________
# dissolution limits and times

def calc_dissolution_limits(time, area):
    if area.ndim > 1:
        index_25 = np.argmin(np.absolute(area-0.75), axis=1)
        index_50 = np.argmin(np.absolute(area-0.50), axis=1)
        index_75 = np.argmin(np.absolute(area-0.25), axis=1)
        index_95 = np.argmin(np.absolute(area-0.05), axis=1)
        index_995 = np.argmin(np.absolute(area-0.005), axis=1)
    else:
        index_25 = np.argmin(np.absolute(area-0.75))
        index_50 = np.argmin(np.absolute(area-0.50))
        index_75 = np.argmin(np.absolute(area-0.25))
        index_95 = np.argmin(np.absolute(area-0.05))
        index_995 = np.argmin(np.absolute(area-0.005))

    return np.vstack(
            (
                time[index_25],
                time[index_50],
                time[index_75],
                time[index_95],
                time[index_995]
            )
        ).T


def calc_dissolution_individual_times_stats(areas, time, limit=0.95):
    areas_minus_limit = np.abs(areas - (1-limit))
    diss_times_indices = np.argmin(areas_minus_limit, axis=1)
    diss_times = time[diss_times_indices]

    times_mean = np.mean(diss_times)
    times_median = np.median(diss_times)
    times_stderr = np.std(diss_times, ddof=1) / np.sqrt(diss_times.size)

    return (times_mean, times_median, times_stderr)


def calc_confidence_interval(mean, stderr, degrees_of_freedom, alpha=0.95):
    interval = t.interval(alpha, degrees_of_freedom)
    return (
        mean + interval[0]*stderr,
        mean + interval[1]*stderr,
    )


def calc_trace_ci(cluster_areas):
    n = cluster_areas.shape[0]
    cluster_avg = np.average(cluster_areas, axis=0)
    cluster_stderr = np.std(cluster_areas, axis=0, ddof=1) / np.sqrt(n)
    cluster_ci_min, cluster_ci_max = calc_confidence_interval(cluster_avg, cluster_stderr, n-1)
    return (cluster_avg, cluster_ci_min, cluster_ci_max)
