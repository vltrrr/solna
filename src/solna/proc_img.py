import numpy as np
from skimage.filters import threshold_sauvola
from skimage.exposure import equalize_hist, equalize_adapthist
from joblib import Parallel, delayed

import matplotlib.pyplot as plt

from solna.common import *


# ____________________________________________________________________________
# image processing

def _apply_threshold_sauvola_single(img, window_size, k):
    np.seterr(invalid='ignore')
    tresh = threshold_sauvola(img, window_size=window_size, k=k)
    tresh_img = img < tresh
    np.seterr(invalid='print')
    return tresh_img


def apply_threshold_sauvola_parallel(img_list, n_jobs=4, window_size=51, k=0.2):
    with Parallel(n_jobs=n_jobs) as parallel:
        return parallel(
            (delayed(_apply_threshold_sauvola_single)(img, window_size=window_size, k=k) for img in img_list)
        )


def apply_threshold_sauvola(img_list, window_size=51, k=0.2):
    tresh_images = []
    for img in img_list:
        binary_img = _apply_threshold_sauvola_single(img, window_size=window_size, k=k)
        tresh_images.append(binary_img)
    return tresh_images


def apply_equalize_hist(img_list, mode='adaptive', clip_limit=0.05):
    if mode == 'adaptive':
        return [equalize_adapthist(img, clip_limit=clip_limit) for img in img_list]
    elif mode == 'global':
        return [equalize_hist(img) for img in img_list]


def count_pixel_sum(img_list):
    counts = [np.sum(img) for img in img_list]
    return np.array(counts)
