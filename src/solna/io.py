import os
import collections

import numpy as np
import scipy.io

from solna.common import *


# ____________________________________________________________________________
# helpers

def filename_remove_suffix(basename):
    lastdot_index = basename.rfind('.')

    if lastdot_index != -1:
        return basename[:lastdot_index]
    else:
        return basename


def get_basename(path):
    return os.path.basename(os.path.normpath(path))


def read_dir_filelist(in_path, suffix_filter='.csv'):
    file_list = sorted(os.listdir(in_path))

    filtered_files = []
    for path in file_list:
        if path.lower().endswith(suffix_filter):
            filtered_files.append(path)

    abs_paths_list = [os.path.join(in_path, fname) for fname in filtered_files]
    name_list = [filename_remove_suffix(fname) for fname in filtered_files]

    return abs_paths_list, name_list


def filter_specialchars(name):
    trans_table = str.maketrans(
        ' ,.+-öäÖÄ',
        '_____oaOA',
        '%&$£@#',  # deleted chars
    )
    return str.translate(name, trans_table)


def filter_specialchars_list(names):
    return [filter_specialchars(name) for name in names]


# ____________________________________________________________________________
# read dissolution data

def read_imagej_csv(path, delim):
    """
    read single imagej file into data frame
    """
    import pandas as pd

    solub_frame = pd.read_csv(path, delim)
    solub_frame.rename(columns={'Label': 'label', 'Area': 'area', '%Area': 'area_frac', 'Slice': 'slice'}, inplace=True)

    return solub_frame


def read_cluster_csv(path):
    import pandas as pd
    solub_frame = pd.read_csv(path, sep=";", header=None, comment="#", skip_blank_lines=True, names=('name', 'cluster'))
    return solub_frame


# ____________________________________________________________________________
# video/image data

def read_video_imageio(in_path):
    import imageio
    from skimage.color import rgb2gray

    video_reader = imageio.get_reader(in_path,  'ffmpeg')
    frame_list = []
    for image in video_reader.iter_data():
        frame_list.append(rgb2gray(image))

    # alternate
    # read direct sequence
    # imageio.mimread(in_path, format='ffmpeg')

    return frame_list


# ____________________________________________________________________________
# npz datasets

def write_npz_dataset(fname, trace_names, trace_frames, mat_format=False):
    """
    write raw npz dataset (separate fibers)
    """
    dataset = {}
    for tr_frame, tr_name in zip(trace_frames, trace_names):
        dataset[tr_name] = tr_frame.values

    if mat_format:
        scipy.io.savemat(fname, dataset)
    else:
        np.savez(fname, **dataset)


def read_npz_dataset(fname):
    """
    read raw npz dataset, a dictionary of ndarray traces for each fiber
    """
    dset_npz = np.load(fname)
    basename = os.path.basename(fname)

    dataset = {}
    for tr_name in dset_npz:
        # to load traces with same name from different files,
        # add load file to the name to yield an unique name for each trace
        tr_unique_name = "{:s}_{:s}".format(basename, tr_name)
        dataset[tr_unique_name] = dset_npz[tr_name]

    dset_npz.close()
    return dataset


def read_npz_datasets_from_dir(in_path):
    file_list, name_list = read_dir_filelist(in_path, suffix_filter='.npz')

    if len(file_list) < 1:
        solna_error('no .npz files found.')

    # ordered dict for datasets to preserve added order for clustering
    dset_raw_combined = collections.OrderedDict()
    auto_cluster_codes = []
    cluster_seq = 0

    for in_file in file_list:
        dset_raw = read_npz_dataset(in_file)
        dset_raw_combined.update(dset_raw)

        # add an array of cluster codes with the current cluster sequence code
        auto_cluster_codes.extend([cluster_seq] * len(dset_raw))
        cluster_seq += 1

    auto_cluster_codes = np.array(auto_cluster_codes)
    return dset_raw_combined, auto_cluster_codes


def write_solna_dataset_matlab(fname, sol_dset, cluster_names):
    """
    write processed dataset (separate fibers)
    """
    export_dict = {}
    export_dict['solna_names'] = sol_dset.names
    export_dict['solna_time'] = sol_dset.time
    export_dict['solna_areas'] = sol_dset.areas
    export_dict['solna_areas_abs'] = sol_dset.areas_abs

    if sol_dset.cluster_avgs is not None:
        export_dict['solna_cluster_avgs'] = sol_dset.cluster_avgs
    if sol_dset.cluster_codes is not None:
        export_dict['solna_cluster_codes'] = sol_dset.cluster_codes
    if cluster_names is not None:
        export_dict['solna_cluster_names'] = cluster_names

    scipy.io.savemat(fname, export_dict)
