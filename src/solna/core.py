import os
import numpy as np

from solna.common import *
from solna import proc, dataset, plot, out_csv, out_msg
from solna import io as sio


# ____________________________________________________________________________
# processing

def prepare_dataset(npz_dataset, conf):
    dset_name = os.path.basename(os.path.normpath(conf.in_path))
    dset_name = sio.filename_remove_suffix(dset_name)
    return dataset.init_dataset(npz_dataset, conf.maxtime, dset_name)


def process_dataset(dset, conf):
    if conf.mavg_enabled:
        msg('smoothing: mavg smooth, window = {:d} pts'.format(conf.mavg_size))
        dset.smooth_mavg(conf.mavg_size)
    else:
        msg('smoothing: no smoothing.')

    dset.comp_avg()
    dset.comp_lowres()
    dset.comp_gradients()
    dset.comp_cumsum()
    dset.comp_dissolution_limits()
    dset.comp_dissolution_individual_times()
    dset.comp_fit_initspeed()
    dset.comp_fit_exp()


def process_clustering(dset, conf):
    if conf.n_clusters >= 1:
        dset.cluster(conf.n_clusters)
    elif conf.cluster_hard > 0:
        dset.cluster_by_hard_cumsum(conf.cluster_hard)
    elif conf.cluster_gradmax > 0:
        dset.cluster_by_gradient(conf.cluster_gradmax)
    elif conf.cluster_time > 0:
        dset.cluster_by_time_cutoff(conf.cluster_time)
    elif conf.cluster_file is not None:
        # read file
        cluster_dframe = sio.read_cluster_csv(conf.cluster_file)
        dset.cluster_by_dframe(cluster_dframe)

    dset.comp_cluster_avg()
    dset.comp_cluster_dissolution_limits()
    dset.comp_cluster_dissolution_individual_times()
    dset.comp_cluster_gradients()
    dset.comp_cluster_exp_fits()
    dset.comp_lowres_cluster()


def check_cluster_names_and_colors(cluster_codes, cluster_names, cluster_colors):
    n_of_clusters = np.amax(cluster_codes) + 1
    if cluster_names is not None and len(cluster_names) < n_of_clusters:
        solna_error('fewer cluster names than the number of clusters.')
    if cluster_colors is not None and len(cluster_colors) < n_of_clusters:
        solna_error('fewer cluster colors than the number of clusters.')


# ____________________________________________________________________________
# output

def write_basic_results(dset, conf):
    msg('fits:')
    out_msg.print_fits(dset)
    out_csv.write_fits(dset, 'solub_fits.csv')

    msg('individual fiber average dissolution time (dissolution time = 95% dissolved)')
    out_msg.print_avg_diss_stats(dset)
    out_csv.write_avg_diss_stats([dset.diss_times_stats], [dset.diss_times_ci95], dset.name, 'diss_stats.csv')

    msg('solubility limits (seconds):')
    out_msg.print_cluster_diss_limits(dset.diss_limits)
    out_csv.write_cluster_diss_limits(dset.diss_limits, dset.name, 'solub_times.csv')

    if conf.csv_avg_enabled:
        out_csv.write_avg_resampled(dset.area_avg_lowres, dset.name, 'traces_avg.csv')
    if conf.csv_lowres_enabled:
        out_csv.write_traces_resampled(dset.areas_lowres, dset.names, 'traces_{:s}.csv'.format(dset.name))


def write_cluster_results(dset, conf):
    if conf.cnames_out_enabled:
        cluster_names = conf.cluster_names
    else:
        cluster_names = None

    msg('\t# cluster averages')
    out_msg.print_cluster_diss_limits(dset.cluster_diss_limits)

    out_csv.write_cluster_diss_limits(
        dset.cluster_diss_limits,
        dset.name,
        'solub_times_cluster.csv',
        cluster_names
    )

    if conf.csv_lowres_enabled:
        out_csv.write_cluster_avgs_resampled(
            dset.cluster_avgs_lowres,
            'traces_clusteravg_{:s}.csv'.format(dset.name),
            cluster_names
        )

    msg('cluster fits:')
    out_msg.print_cluster_fits(dset)
    out_csv.write_cluster_fits(
        dset.cluster_fit_exp_coefs,
        dset.cluster_fit_exp_stderrs,
        dset.name,
        'solub_fits_cluster.csv',
    )

    msg('cluster individual fiber average dissolution time (dissolution time = 95% dissolved):')
    out_msg.print_cluster_avg_diss_stats(dset)

    out_csv.write_avg_diss_stats(
        dset.cluster_diss_times_stats,
        dset.cluster_diss_times_ci95,
        dset.name,
        'diss_stats_cluster.csv',
        cluster_names
    )

    msg('cluster sizes (1 = fastest):')
    out_msg.print_cluster_sizes(dset.cluster_codes)
    if conf.cnames_out_enabled:
        out_csv.write_cluster_sizes(dset.cluster_codes, dset.name, 'cluster_sizes.csv', conf.cluster_names)
    else:
        out_csv.write_cluster_sizes(dset.cluster_codes, dset.name, 'cluster_sizes.csv')

    msg('cluster contents (1 = fastest):')
    out_msg.print_cluster_sample_codes(dset.names, dset.cluster_codes)


# ____________________________________________________________________________
# plots

def plot_basic(dset, conf):
    # relative traces
    plot.dset_all(dset, dset.areas, conf.plt_legend)
    plot.plot_avg(dset.time, dset.area_avg)
    if conf.plt_initspeed:
        plot.plot_fit(dset.time, dset.fit_initspeed_line)
        plot.plot_vline(dset.diss_limits[0][4])
    if conf.plt_expfit:
        plot.plot_fit(dset.time, dset.fit_exp_line)
    plot.setup_labels(title='All traces, relative', ylabel='Relative area')
    plot.setup_ylimit()
    plot.setup_xlimit(0.0, dset.time_max)
    if conf.plt_log_y:
        plot.setup_log_y()
        plot.setup_ylimit(min_y=0.001)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_avg')

    # absolute traces
    plot.dset_all(dset, dset.areas_abs, conf.plt_legend)
    plot.plot_avg(dset.time, dset.area_avg_abs)
    if conf.plt_initspeed:
        plot.plot_fit(dset.time, dset.fit_initspeed_line_abs)
    if conf.plt_expfit:
        plot.plot_fit(dset.time, dset.fit_exp_line_abs)
    plot.setup_labels(title='All traces, absolute', ylabel='Absolute area (pixels)')
    plot.setup_ylimit(max_y=np.amax(dset.areas_abs))
    plot.setup_xlimit(0.0, dset.time_max)
    if conf.plt_log_y:
        plot.setup_log_y()
        plot.setup_ylimit(min_y=1.0, max_y=np.amax(dset.areas_abs))
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_abs')


def plot_cluster(dset, conf):
    # cluster avg with traces
    plot.dset_all(dset, dset.areas, conf.plt_legend)
    plot.plot_cluster_avg(dset.time, dset.cluster_avgs, conf.cluster_names, conf.cluster_colors)
    if conf.plt_expfit:
        for fit_line in dset.cluster_fit_exp_lines:
            plot.plot_fit(dset.time, fit_line)
    plot.setup_labels(title='Cluster traces and cluster averages')
    plot.setup_ylimit()
    plot.setup_xlimit(0.0, dset.time_max)
    if conf.plt_log_y:
        plot.setup_log_y()
        plot.setup_ylimit(min_y=0.001)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_cavg')

    # plain cluster averages
    if conf.plt_ci:
        # the interval areas need low resolution time / datapoints
        # as they are not simplified by matplotlib. so thats why this mess.
        plot.plot_cluster_avg(
            dset.time,
            dset.cluster_avgs,
            conf.cluster_names,
            conf.cluster_colors,
            thinlines=True,
            interval_time=dset.time_lowres,
            interval_min=dset.cluster_avgs_ci_min,
            interval_max=dset.cluster_avgs_ci_max)
    else:
        plot.plot_cluster_avg(
            dset.time,
            dset.cluster_avgs,
            conf.cluster_names,
            conf.cluster_colors,
            thinlines=True)
    if conf.plt_expfit:
        for fit_line in dset.cluster_fit_exp_lines:
            plot.plot_fit(dset.time, fit_line)
    plot.setup()
    plot.setup_labels(title='Cluster averages')
    plot.setup_ylimit()
    plot.setup_xlimit(0.0, dset.time_max)
    if conf.plt_log_y:
        plot.setup_log_y()
        plot.setup_ylimit(min_y=0.001)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_cavg_plain')


def plot_gradients(dset, conf):
    plot.dset_all(dset, dset.area_gradients, conf.plt_legend)
    plot.setup_labels(title='Gradients (relative)', ylabel='Dissolution speed')
    plot.setup_ylimit()
    plot.setup_xlimit(0.0, dset.time_max)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_grad_rel')


def plot_cluster_gradients(dset, conf):
    # cluster gradients
    plot.plot_cluster_avg(
        dset.time,
        dset.cluster_avgs_gradients,
        conf.cluster_names,
        conf.cluster_colors,
        thinlines=True)
    plot.setup()
    plot.setup_labels(title='Cluster gradients (relative)', ylabel='Dissolution speed')
    plot.setup_xlimit(0.0, dset.time_max)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_cavg_grad')


def plot_cumsum(dset, conf):
    plot.dset_cumsum(dset, conf.plt_legend)
    plot.setup_labels(title='Cumulative sum', ylabel='Sum')
    plot.setup_ylimit()
    plot.setup_xlimit(0.0, dset.time_max)
    plot.render_out(conf, dset.name, conf.plt_x, conf.plt_y, '_cumsum')
