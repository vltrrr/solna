
import numpy as np
import pandas as pd

from solna.common import *
from solna import io as sio
from solna import proc, proc_img, proc_pre


# ____________________________________________________________________________
# cmdline / conf

def add_default_args(parser):
    parser.add_argument(
        '-framei',
        type=float,
        help='original/force frame interval',
        default=None)
    parser.add_argument(
        '-framer',
        type=float,
        help='original/force frame rate',
        default=None)


def parse_frame_interval(args, default_interval=None):
    args.frame_interval = None
    if args.framer is not None and args.framer > 0:
        args.frame_interval = 1.0/args.framer
    elif args.framei is not None and args.framei > 0:
        args.frame_interval = args.framei
    elif default_interval:
        args.frame_interval = default_interval

    return args


# ____________________________________________________________________________
# imagej csv operations

def read_imagej_csv_list(file_list, delim):
    """
    read all csv files in the file list
    """
    if len(file_list) < 1:
        solna_error('no .csv files found.')

    dframe_list = []
    for in_path in file_list:
        msg('\t{:s}'.format(in_path))
        dframe = sio.read_imagej_csv(in_path, delim)
        dframe_list.append(dframe)

    return dframe_list


def preprocess_imagej_dframes(dframe_list, frame_interval):
    preproc_frames = []
    for dframe in dframe_list:
        if not frame_interval:
            dframe = proc_pre.parse_timeaxis_imagej(dframe)
        else:
            dframe = proc_pre.calc_timeaxis(dframe, frame_interval)

        dframe = proc_pre.calc_area_abs_imagej(dframe)
        dframe = proc_pre.normalize_area_frac(dframe)

        # filter only needed cols in right order
        dframe = dframe[['sec', 'area_frac', 'area_abs']]
        preproc_frames.append(dframe)

    return preproc_frames


# ____________________________________________________________________________
# video operations

def read_video_list(file_list):
    dframe_list = []
    for in_path in file_list:
        msg('\treading {:s}'.format(in_path))
        img_list = read_video_file(in_path)
        msg('\textracting ({:d} frames)'.format(len(img_list)))
        counts, img_threshold = _extract_pixel_counts(img_list)
        dframe = pd.DataFrame({'area_abs': counts})
        dframe_list.append(dframe)
    return dframe_list


def read_video_file(in_path, skip_start=0, skip_end=1):
    img_list = sio.read_video_imageio(in_path)
    img_list = img_list[skip_start:-skip_end]
    return img_list


def _extract_pixel_counts(img_list):
    img_threshold = proc_img.apply_threshold_sauvola_parallel(img_list)
    counts = proc_img.count_pixel_sum(img_threshold)
    return counts, img_threshold


def preprocess_video_dframes(dframe_list, frame_interval):
    preproc_dframes = []
    for dframe in dframe_list:
        dframe = proc_pre.calc_area_frac(dframe)
        dframe = proc_pre.normalize_area_frac(dframe)
        dframe = proc_pre.normalize_area_abs(dframe)
        dframe = proc_pre.calc_timeaxis(dframe, frame_interval)

        # reorganize colums to expected order
        dframe = dframe[['sec', 'area_frac', 'area_abs']]
        preproc_dframes.append(dframe)
    return preproc_dframes
