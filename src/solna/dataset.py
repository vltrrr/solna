import os
import argparse

import numpy as np

from solna.common import *
from solna import proc


def init_dataset(npz_dataset, time_max_limit, dset_name):
    """
    init dataset object from raw npz dataset (a dictionary of ndarrays for each fiber)
    """

    # create/resample time axis
    if time_max_limit >= 1:
        time_max = time_max_limit
    else:
        time_max = _find_maxtime_from_dataset(npz_dataset)
    time_axis = proc.resample_timeaxis(time_max)

    # resample all data & create arrays
    names, areas, areas_abs = _resample_dataset(npz_dataset, time_axis)

    # prepare solna dataset
    sdset = solna_dataset(names, time_axis, areas)
    sdset.areas_abs = areas_abs
    sdset.time_max = time_max
    sdset.name = dset_name

    return sdset


def _find_maxtime_from_dataset(npz_dataset):
    """
    find maximum time point in the dataset
    """
    time_max = 0

    for tr_name, tr_data in npz_dataset.items():
        found_max = np.amax(tr_data[:, 0])
        if found_max > time_max:
            time_max = found_max

    return time_max


def _resample_dataset(npz_dataset, time_axis):
    """
    resample the dataset
    """
    names = []
    area_list = []
    area_list_abs = []

    for tr_name, tr_data in npz_dataset.items():
        names.append(tr_name)
        msg("\t{:s}".format(tr_name))

        area = proc.resample_area(time_axis, tr_data[:, 0], tr_data[:, 1])
        area_abs = proc.resample_area(time_axis, tr_data[:, 0], tr_data[:, 2])
        area_list.append(area)
        area_list_abs.append(area_abs)

    areas = np.vstack(area_list)
    areas_abs = np.vstack(area_list_abs)

    return names, areas, areas_abs


class solna_dataset(object):
    """
    main class holding the dataset and methods for operations.
    as this is now grown so large, one or two pandas dataframes would probably
    have been a better idea. no time to refactor now :)
    """
    def __init__(self, names, time_axis, areas):
        if len(names) != areas.shape[0]:
            solna_error("mismatch in dataset: len of names != n of traces")
        if time_axis.size != areas.shape[1]:
            solna_error("mismatch in dataset: len of time_axis != areas")

        self.names = names
        self.name = None  # the name of the dataset

        self.time_max = 0.0
        self.time = time_axis
        self.areas = areas
        self.areas_abs = None
        self.areas_lowres = None

        self.area_avg = None
        self.area_avg_lowres = None
        self.area_avg_abs = None
        self.area_size_avg = None
        self.area_size_median = None
        self.area_cumsum = None
        self.area_gradients = None
        self.diss_limits = None

        self.fit_initspeed_line = None
        self.fit_initspeed = None
        self.fit_intercept = None
        self.fit_initspeed_line_abs = None
        self.fit_initspeed_abs = None
        self.fit_intercept_abs = None

        self.cluster_codes = None
        self.cluster_avgs = None
        self.cluster_avgs_lowres = None
        self.cluster_diss_limits = None
        self.cluster_avgs_gradients = None

        self.plot_colors = None

    def smooth_mavg(self, mavg_size):
        n_samples = self.areas.shape[0]
        for i in range(n_samples):
            self.areas[i] = proc.smooth_mavg(self.areas[i], win_len=mavg_size)

        for i in range(n_samples):
            self.areas_abs[i] = proc.smooth_mavg(self.areas_abs[i], win_len=mavg_size, post_normz=False)

    def comp_avg(self):
        self.area_avg = np.average(self.areas, axis=0)
        self.area_avg_abs = np.average(self.areas_abs, axis=0)

        self.area_size_avg = self.area_avg_abs[0]
        self.area_size_median = np.median(self.areas_abs, axis=0)[0]

    def comp_lowres(self):
        self.time_lowres, self.area_avg_lowres = proc.resample_area_lowres(self.time, self.area_avg)

        n_samples = self.areas.shape[0]
        lowres_traces = []
        for i in range(n_samples):
            lowres_traces.append(proc.resample_area_lowres(self.time, self.areas[i])[1])

        self.areas_lowres = np.vstack(lowres_traces)

    def comp_lowres_cluster(self):
        n_clusters = self.cluster_avgs.shape[0]
        lowres_cluster_avgs = []
        for i in range(n_clusters):
            lowres_cluster_avgs.append(proc.resample_area_lowres(self.time, self.cluster_avgs[i])[1])

        self.cluster_avgs_lowres = np.vstack(lowres_cluster_avgs)

    def comp_cumsum(self):
        norm_coef = (1.0 / (self.areas.shape[1]-1)) * np.amax(self.time)
        self.area_cumsum = np.cumsum(self.areas, axis=1) * norm_coef

    def comp_dissolution_limits(self):
        self.diss_limits = proc.calc_dissolution_limits(self.time, self.area_avg)

    def comp_dissolution_individual_times(self, limit=0.95):
        self.diss_times_stats = proc.calc_dissolution_individual_times_stats(self.areas, self.time, limit)
        self.diss_times_ci95 = proc.calc_confidence_interval(
            self.diss_times_stats[0],
            self.diss_times_stats[2],
            self.areas.shape[0]-1,
            alpha=limit,
        )

    def comp_gradients(self):
        # to get more meaningful units (1/1000th part/sec)
        self.area_gradients = proc.gradients(self.areas) * 1000

    def comp_fit_initspeed(self):
        coefs = proc.fit_initspeed(self.area_avg, self.time, 0.80)
        self.fit_initspeed_line = coefs[0] * self.time + coefs[1]
        self.fit_initspeed = -coefs[0] * 100
        self.fit_intercept = -coefs[1]/coefs[0]

        coefs = proc.fit_initspeed(self.area_avg_abs, self.time, 0.80)
        self.fit_initspeed_line_abs = coefs[0] * self.time + coefs[1]
        self.fit_initspeed_abs = -coefs[0]
        self.fit_intercept_abs = -coefs[1] / coefs[0]

    def comp_fit_exp(self):
        a, t0 = proc.fit_exp(self.area_avg, self.time)
        self.fit_exp_coefs = (a, t0)
        self.fit_exp_line = proc.calc_exp_line(a, t0, self.time)
        self.fit_exp_stderr = proc.calc_fit_stderr(self.fit_exp_line, self.area_avg)

        a, t0 = proc.fit_exp(self.area_avg_abs, self.time)
        self.fit_exp_coefs_abs = (a, t0)
        self.fit_exp_line_abs = proc.calc_exp_line(a, t0, self.time)
        self.fit_exp_stderr_abs = proc.calc_fit_stderr(self.fit_exp_line_abs, self.area_avg_abs)

    def cluster(self, n_of_clusters):
        integrals = self.area_cumsum[:, -1]
        self.cluster_codes = proc.cluster_k_means(integrals, n_of_clusters)

    def cluster_by_hard_cumsum(self, cumsum_limit):
        integrals = self.area_cumsum[:, -1]
        self.cluster_codes = np.where(integrals > cumsum_limit, 1, 0)

    def cluster_by_gradient(self, max_rate):
        grad_max = np.amax(self.area_gradients, axis=1)
        self.cluster_codes = np.where(grad_max > max_rate, 0, 1)

    def cluster_by_time_cutoff(self, cutoff_timepoint, cutoff=0.1):
        cutoff_point = np.argmin(np.abs(self.time - cutoff_timepoint))
        areas_at_cutoff = self.areas[:, cutoff_point]
        self.cluster_codes = np.where(areas_at_cutoff > cutoff, 1, 0)

    def cluster_by_dframe(self, cluster_dframe):
        name_series = cluster_dframe['name'].str.strip()

        # init to cluster 0
        self.cluster_codes = np.zeros(len(self.names), dtype=np.int)

        # FIXME O(n²), better version to match using vectorization / sorted lists
        for prefix_i in range(name_series.size):
            name_prefix = name_series[prefix_i]
            for names_i in range(len(self.names)):
                if self.names[names_i].startswith(name_prefix):
                    self.cluster_codes[names_i] = cluster_dframe['cluster'][prefix_i]

    def comp_cluster_avg(self):
        cluster_avgs = []
        cluster_ci_min = []
        cluster_ci_max = []

        for i in range(np.amax(self.cluster_codes)+1):
            cluster_areas = self.areas[self.cluster_codes == i, :]
            cluster_avg, ci_min, ci_max = proc.calc_trace_ci(cluster_areas)
            cluster_avgs.append(cluster_avg)
            # resample CI:s lower resolution for plotting
            cluster_ci_min.append(proc.resample_area_lowres(self.time, ci_min)[1])
            cluster_ci_max.append(proc.resample_area_lowres(self.time, ci_max)[1])

        self.cluster_avgs = np.array(cluster_avgs)
        self.cluster_avgs_ci_min = np.array(cluster_ci_min)
        self.cluster_avgs_ci_max = np.array(cluster_ci_max)

    def comp_cluster_dissolution_individual_times(self):
        self.cluster_diss_times_stats = []
        self.cluster_diss_times_ci95 = []

        for i in range(np.amax(self.cluster_codes) + 1):
            cluster_areas = self.areas[self.cluster_codes == i, :]
            stats = proc.calc_dissolution_individual_times_stats(cluster_areas, self.time)
            ci = proc.calc_confidence_interval(stats[0], stats[2], cluster_areas.shape[0]-1)

            self.cluster_diss_times_stats.append(stats)
            self.cluster_diss_times_ci95.append(ci)

    def comp_cluster_exp_fits(self):
        self.cluster_fit_exp_coefs = proc.calc_cluster_exp_fits(self.cluster_avgs, self.time)
        self.cluster_fit_exp_lines = proc.calc_cluster_exp_fit_lines(self.cluster_fit_exp_coefs, self.time)
        self.cluster_fit_exp_stderrs = proc.calc_cluster_exp_fit_stderrs(self.cluster_fit_exp_lines, self.cluster_avgs)

    def comp_cluster_dissolution_limits(self):
        self.cluster_diss_limits = proc.calc_dissolution_limits(self.time, self.cluster_avgs)

    def comp_cluster_gradients(self):
        self.cluster_avgs_gradients = proc.gradients(self.cluster_avgs) * 1000
