import os
import platform
import argparse

import numpy as np
import pandas as pd

from solna.common import *
from solna import io as sio
from solna import proc, core_extract


# ____________________________________________________________________________
# cmdline / conf

def parse_cmdline():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'in_path',
        help='input file or directory',
        metavar='input')

    core_extract.add_default_args(parser)
    args = parser.parse_args()
    core_extract.parse_frame_interval(args, default_interval=1.0)

    return args


# ____________________________________________________________________________
# main

@solna_execute
def main():
    msg('solna_video_extract ({:s} python {:s} {:s})'.format(
            SOLNA_VERSION,
            platform.python_version(),
            platform.system())
        )
    msg('')

    args = parse_cmdline()
    if args.frame_interval is not None:
        msg('frame interval: {:f}'.format(args.frame_interval))

    msg('extracting videos:')

    if os.path.isdir(args.in_path):
        file_list, name_list = sio.read_dir_filelist(args.in_path, suffix_filter='.avi')
        dframe_list = core_extract.read_video_list(file_list)
    else:
        dframe_list = core_extract.read_video_list([args.in_path])
        name_list = [sio.get_basename(args.in_path)]

    dframe_list = core_extract.preprocess_video_dframes(dframe_list, args.frame_interval)
    name_list = sio.filter_specialchars_list(name_list)
    fname = sio.get_basename(args.in_path)
    sio.write_npz_dataset(fname, name_list, dframe_list)


if __name__ == '__main__':
    main()
