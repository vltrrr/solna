import numpy as np
import matplotlib
import matplotlib.animation as animation
from matplotlib import pyplot as plt

from solna.common import *


# ____________________________________________________________________________
# plot

def setup(legend=False):
    """ setup default labels, grid & legend """
    plt.xlabel('Time (sec)')
    plt.ylabel('Relative area')
    plt.grid()

    if legend:
        plt.legend(prop={'size': 8})


def setup_log_y(base=10):
    plt.semilogy(basey=base, subsy=[2, 3, 4, 5, 6, 7, 8, 9])


def setup_ylimit(min_y=0.0, max_y=1.0):
    plt.ylim(min_y, max_y)


def setup_xlimit(min_x, max_x):
    plt.xlim(min_x, max_x)


def setup_labels(title="", xlabel='Time (sec)', ylabel='Relative area'):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def plot_solub(time, area, linestyle='-', lw=1.0, color=None, label='', alpha=1.0):
    line, = plt.plot(time, area)
    if color is not None:
        line.set_color(color)
    line.set_linewidth(lw)
    # line.set_marker('o')
    line.set_linestyle(linestyle)
    line.set_markersize(5)
    line.set_label(label)
    line.set_alpha(alpha)


def plot_interval(time, limit_lo, limit_hi, color=None):
    plt.fill_between(time, limit_hi, limit_lo, facecolor=color, alpha=0.15)


def plot_solub_arr(time, areas, colors, linestyle='-'):
    n_samples = areas.shape[0]
    for i in range(n_samples):
        plot_solub(time, areas[i], color=colors[i], linestyle=linestyle)


def plot_solub_arr_legend(time, areas, names):
    n_samples = areas.shape[0]
    for i in range(n_samples):
        plot_solub(time, areas[i], label=names[i])


def plot_avg(time, avg):
    plot_solub(time, avg, color="black", linestyle='--', lw=4.0)


def plot_fit(time, fit, linestyle='-'):
    plot_solub(time, fit, color="black", linestyle=linestyle, lw=2.0, alpha=1.0)


def plot_vline(xcoord):
    plt.plot((xcoord, xcoord), (0.0, 1.0), color="black", lw=2.0)


def plot_cluster_avg(
        time,
        cluster_avgs,
        cluster_names=None,
        cluster_colors=None,
        thinlines=False,
        interval_time=None,
        interval_min=None,
        interval_max=None):
    # construct colors for cluster averages and plot
    codes = np.arange(len(cluster_avgs))

    # if not supplied, create cluster colors and labels from number codes
    if cluster_colors is None:
        cluster_colors = create_colors_codes(codes)
    if cluster_names is None:
        cluster_names = [str(c+1) for c in codes]

    linestyle = '--'
    lw = 4.0

    if thinlines:
        linestyle = '-'
        lw = 3.0

    for i in range(len(cluster_avgs)):
        plot_solub(time, cluster_avgs[i], color=cluster_colors[i], linestyle=linestyle, lw=lw, label=cluster_names[i])
        if interval_min is not None and interval_max is not None and interval_time is not None:
            plot_interval(interval_time, interval_min[i], interval_max[i], color=cluster_colors[i])

    # allways plot legend
    plt.legend(prop={'size': 10})


# ___________________________________________________________________________
# dset plot

def dset_all(dset, areas, plt_legend):
    if plt_legend:
        plot_solub_arr_legend(dset.time, areas, dset.names)
    else:
        plot_solub_arr(dset.time, areas, dset.plot_colors)
    setup(legend=plt_legend)


def dset_cumsum(dset, plt_legend):
    if plt_legend:
        plot_solub_arr_legend(dset.time, dset.area_cumsum, dset.names)
    else:
        plot_solub_arr(dset.time, dset.area_cumsum, dset.plot_colors)
    setup(legend=plt_legend)


# ____________________________________________________________________________
# colors

def create_colors_codes(codes, palette=None):
    n_of_clusters = np.amax(codes+1)

    if palette is None:
        if n_of_clusters <= 2:
            palette = ["red", "blue"]
        elif n_of_clusters <= 3:
            palette = ["red", "green", "blue"]
        elif n_of_clusters <= 4:
            palette = ["red", "orange", "green", "blue"]
        elif n_of_clusters <= 5:
            palette = ["red", "orange", "green", "violet", "blue"]
        elif n_of_clusters <= 6:
            palette = ["red", "orange", "gray", "green", "violet", "blue"]
        else:
            colors = np.linspace(0, 1, n_of_clusters)
            palette = plt.cm.brg(colors)

    if len(palette) < n_of_clusters:
        solna_error("cluster/palette colors length less than n of clusters.")

    return [palette[c] for c in codes.tolist()]


def create_colors_single(n_samples):
    return ['r'] * n_samples


# ____________________________________________________________________________
# image sequences

def plot_image_sequence(img_list, interval=50, cmap='magma'):
    fig = plt.figure()
    plt_images = []
    for img in img_list:
        plt_img = plt.imshow(img, cmap=cmap, animated=True)
        plt_images.append([plt_img])

    ani = animation.ArtistAnimation(fig, plt_images, interval=interval)

    plt.tight_layout()
    plt.show()

# ____________________________________________________________________________
# render

def render_out(conf, outfile, width, height, suffix=""):
    if conf.out_file_png:
        render_file(outfile + suffix, width, height, format="png")
    if conf.out_file_pdf:
        render_file(outfile + suffix, width, height, format="pdf")
    if conf.out_plot:
        render()
    reset()


def render():
    plt.show()


def render_file(fname, width, height, format="png"):
    figure = plt.gcf()
    figure.set_frameon(True)
    figure.set_size_inches((width/25.4, height/25.4))
    plt.tight_layout()

    if format == "png":
        plt.savefig(fname + ".png", dpi=75, format="png")
    elif format == "pdf":
        plt.savefig(fname + ".pdf", dpi=75, format="pdf")
    else:
        vd_error("invalid format: " + format)


def reset():
    plt.clf()
