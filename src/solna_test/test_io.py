import pytest
import numpy as np
import math

from solna import io as sio

# ____________________________________________________________________________
# helpers

def test_filter_specialchars():
    filtered = sio.filter_specialchars('file_abcd+-&%_öä_BOOM.kablam.csv')
    assert filtered == 'file_abcd___oa_BOOM_kablam_csv'

