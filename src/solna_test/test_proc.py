import pytest
import numpy as np
import math

from solna import proc

# ____________________________________________________________________________
# process

def test_smooth_mavg():
    time = np.arange(0, 11, dtype='float64')
    area = np.array(
        [ 1.0, 0.9, 0.8, 0.5, 0.4, 0.4, 0.3, 0.3, 0.2, 0.2, 0.05 ],
    )
    smoothed = proc.smooth_mavg(area, 3)

    assert area.shape == smoothed.shape
    assert smoothed[0] == pytest.approx(1.0)


# ____________________________________________________________________________
# fit

def test_crop_fit_area():
    area = np.linspace(1.0, 0.0, 11)
    time = np.linspace(0.0, 1.0, 11)

    area_crop, time_crop = proc.crop_fit_area(area, time, limit_lo=0.8)
    assert area_crop == pytest.approx(np.array([1.0, 0.9, 0.8]))
    assert time_crop == pytest.approx(np.array([0.0, 0.1, 0.2]))

    area_crop, time_crop = proc.crop_fit_area(area, time, limit_hi=0.9, limit_lo=0.5)
    assert area_crop == pytest.approx(np.array([0.9, 0.8, 0.7, 0.6, 0.5]))
    assert time_crop == pytest.approx(np.array([0.1, 0.2, 0.3, 0.4, 0.5]))


def test_fit_initspeed():
    time = np.arange(0, 11, dtype='float64')
    area = np.array(
        [ 1.0, 0.9, 0.8, 0.5, 0.4, 0.4, 0.3, 0.3, 0.2, 0.2, 0.05 ],
    )
    k, c = proc.fit_initspeed(area, time, 0.8)

    assert k == pytest.approx(-0.1)
    assert c == pytest.approx(1.0)


def test_fit_exp():
    time = np.arange(0, 11, dtype='float64')
    area = np.exp(-np.linspace(0.0, 1.0, 11))

    a, t0 = proc.fit_exp(area, time)
    assert a == pytest.approx(1.0)
    assert t0 == pytest.approx(10.0)


def test_calc_exp_line():
    time = np.linspace(0.0, 1.0, 11)

    expline = proc.calc_exp_line(1.0, 1.0, time)
    assert expline[0] == pytest.approx(1.0)
    assert expline[10] == pytest.approx(1.0/np.e)


def test_calc_fit_stderr():
    fit = np.array([1.0, 0.5, 1.5, 1.0])
    data = np.array([1.0, 1.0, 1.0, 1.0])

    stderr = proc.calc_fit_stderr(fit, data)
    assert stderr == pytest.approx(0.25)


def test_calc_cluster_exp_fits():
    time = np.linspace(0.0, 1.0, 11)
    area = np.exp(-time)
    area2 = 2.0 * np.exp(-time/0.5)

    traces = np.vstack([area, area2])
    fit_coefs = proc.calc_cluster_exp_fits(traces, time)

    assert fit_coefs[0][0] == pytest.approx(1.0)
    assert fit_coefs[0][1] == pytest.approx(1.0)
    assert fit_coefs[1][0] == pytest.approx(2.0)
    assert fit_coefs[1][1] == pytest.approx(0.5)


def test_calc_cluster_exp_fit_lines():
    coefs = np.array( [(1.0, 1.0)] )
    time = np.linspace(0.0, 1.0, 11)

    fit_lines = proc.calc_cluster_exp_fit_lines(coefs, time)
    expline = fit_lines[0]
    assert expline[0] == pytest.approx(1.0)
    assert expline[10] == pytest.approx(1.0/np.e)


# ____________________________________________________________________________
# cluster

def test_cluster_k_means():
    time = np.arange(0, 11, dtype='float64')
    numbers = np.array(
        [ 1.0, 0.95, 0.90, 0.92, 0.2, 0.25, 0.22, 0.23 ]
    )
    cluster_codes = proc.cluster_k_means(numbers, 2)

    assert cluster_codes == pytest.approx([1, 1, 1, 1, 0, 0, 0, 0])


# ____________________________________________________________________________
# dissolution limits and times

def test_calc_dissolution_limits():
    time = np.arange(0, 11, dtype='float64')
    areas = np.array(
        [ 1.0, 0.9, 0.75, 0.7, 0.6, 0.5, 0.4, 0.3, 0.25, 0.05, 0.00 ]
    )
    diss_limits = proc.calc_dissolution_limits(time, areas)
    assert diss_limits[0][0] == pytest.approx(2.0)
    assert diss_limits[0][1] == pytest.approx(5.0)
    assert diss_limits[0][2] == pytest.approx(8.0)
    assert diss_limits[0][3] == pytest.approx(9.0)
    assert diss_limits[0][4] == pytest.approx(10.0)


def test_calc_dissolution_individual_times_stats():
    areas = np.array(
        [
            [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05 ],
            [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05 ],
            [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.15, 0.10, 0.05, 0.0 ],
            [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.15, 0.0, 0.0, 0.0 ],
        ]
    )
    time = np.arange(0, 11, dtype='float64')
    stats = proc.calc_dissolution_individual_times_stats(areas, time)

    assert stats[0] == pytest.approx(9.25)
    assert stats[1] == pytest.approx(9.5)
    assert stats[2] == pytest.approx(0.4787135538781690)


def test_calc_confidence_interval():
    ci = proc.calc_confidence_interval(0, 1.0, 3)
    assert ci[0] == pytest.approx(-3.18245, 1e-5)
    assert ci[1] == pytest.approx(+3.18245, 1e-5)

    ci = proc.calc_confidence_interval(1, 0.1, 10)
    assert ci[0] == pytest.approx(1-0.222814, 1e-5)
    assert ci[1] == pytest.approx(1+0.222814, 1e-5)


def test_calc_trace_ci():
    areas = np.array(
        [
            [ 0.8, 0.8, 0.8, 0.8, 0.4, 0.3 ],
            [ 0.8, 0.7, 0.6, 0.5, 0.4, 0.3 ],
            [ 0.8, 0.5, 0.5, 0.5, 0.4, 0.3 ],
        ]
    )
    cluster_avg, cluster_ci_min, cluster_ci_max = proc.calc_trace_ci(areas)

    assert cluster_avg == pytest.approx([ 0.8, 0.66666666666, 0.6333333333333, 0.6, 0.4, 0.3 ])
    assert cluster_ci_min[0] == pytest.approx(0.8)
    assert cluster_ci_max[0] == pytest.approx(0.8)
    assert cluster_ci_min[4] == pytest.approx(0.4)
    assert cluster_ci_max[4] == pytest.approx(0.4)

    stderr_3 = 0.173205/math.sqrt(3)
    avg_3 = 0.6
    assert cluster_ci_max[3] == pytest.approx(avg_3+(4.303*stderr_3), 1e-4)

