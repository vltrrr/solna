import pytest
import numpy as np
import math

from solna import proc_img

# ____________________________________________________________________________
# process

@pytest.fixture
def thresh_img():
    return np.array(
        [
            [ 0.7, 0.8, 0.8, 0.8, 0.7, 0.7 ],
            [ 0.8, 0.9, 0.9, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.1, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.1, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.1, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.1, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.9, 0.9, 0.9, 0.8 ],
            [ 0.8, 0.9, 0.9, 0.9, 0.9, 0.8 ],
            [ 0.7, 0.8, 0.8, 0.8, 0.7, 0.7 ],
        ]
    )


def test_apply_threshold_sauvola_single(thresh_img):
    thresh_mask = proc_img._apply_threshold_sauvola_single(thresh_img, window_size=3, k=0.2)

    assert np.sum(thresh_mask) == 4
    assert np.sum(thresh_img[thresh_mask]) == 0.4


def test_apply_threshold_sauvola_parallel(thresh_img):
    thresh_mask_list = proc_img.apply_threshold_sauvola_parallel([thresh_img], window_size=3, k=0.2)

    assert np.sum(len(thresh_mask_list)) == 1
    assert np.sum(thresh_mask_list[0]) == 4


def test_apply_threshold_sauvola(thresh_img):
    thresh_mask_list = proc_img.apply_threshold_sauvola([thresh_img], window_size=3, k=0.2)

    assert np.sum(len(thresh_mask_list)) == 1
    assert np.sum(thresh_mask_list[0]) == 4


def test_apply_apply_equalize_hist(thresh_img):
    hist_img_list = proc_img.apply_equalize_hist([thresh_img], mode='global')
    assert np.sum(len(hist_img_list)) == 1
    assert hist_img_list[0][1][3] == pytest.approx(1.0)