from setuptools import setup, find_packages

from codecs import open
from os import path

setup(
    name='solna',
    version='0.6.4',

    description='Solna solubility analysis',
    long_description="Solna microscopy analysis software for fiber dissolution",
    url='https://vltr.fi',
    author='Valtteri Mäkelä',
    author_email='valtteri.makela@iki.fi',
    license='BSD',

    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD License',

        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Visualization',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],


    packages=find_packages(exclude=['solna_test']),

    install_requires=[
        'numpy>=1.14.0',
        'scipy>=1.0.0',
        'matplotlib>=2.1.0',
        'pandas>=0.20.0',
        'joblib>=0.11',
        'scikit-image>=0.13.1',
        'imageio>=2.2.0',
    ],

    entry_points={
        'console_scripts': [
            'solna_extr=solna.main_extract:main',
            'solna_cltempl=solna.main_extract:main_cluster_template',
            'solna_vextr=solna.main_vextract:main',
            'solna_vview=solna.main_vview:main',
            'solna_proc=solna.main_proc:main',
        ],
    },
)
