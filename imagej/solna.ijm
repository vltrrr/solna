macro "threshold maxent measure [f7]" {
	setSlice(1);
	setAutoThreshold("MaxEntropy");
	
	do_measr()
	write_measr();
}

macro "threshold yen measure [f8]" {
	setSlice(1);
	setAutoThreshold("Yen");

	do_measr()
	write_measr();
}

macro "threshold 220 measure" {
	setThreshold(0, 220);

	do_measr()
	write_measr();
}

function do_measr() {
	run("Clear Results");

	// set cols for Measure
	//additional cols: min mean
	run("Set Measurements...", "area area_fraction stack display redirect=None decimal=6");
	for (n=1; n<=nSlices; n++) {
		setSlice(n);
		run("Measure");
	}
	setSlice(1);
}

function write_measr() {
	fname = getInfo("image.filename")
	fname_strip = substring(fname, 0, lastIndexOf(fname,"."))
	out_path = getInfo("image.directory") + fname_strip + ".csv"
	
	saveAs("measurements", out_path)
}

macro "rm slices before [f4]" {
	last = getSliceNumber()-1;
	//print("last=" + last)
	run("Slice Remover", "first=1 last=" + last + " increment=1");
	//alternates:
    //run("Make Substack...", "slices=2-89");
    //run("Slice Keeper", "first=2 last=87 increment=1");
    
    setSlice(1);
}

macro "rm slices after [f5]" {
	first = getSliceNumber()+1;
	run("Slice Remover", "first=" + first + " last=" + nSlices() + " increment=1");
	
	setSlice(1);
}

macro "create white outside [f6]" {
	run("Make Inverse");
	setForegroundColor(255, 255, 255);
	run("Fill", "stack");
	run("Select None");
}
